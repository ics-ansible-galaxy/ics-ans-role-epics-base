# ics-ans-role-epics-base

Ansible role to install epics-base.

## Role Variables

```yaml
# Variable to pass extra OS packages to be installed if required
epics_base_extra_packages: []
# Set to true to activate the epics environment in the profile
epics_base_activate_profile: false
# Conda packages to install in the epics environment
epics_base_conda_packages:
  - epics-base=7.0.3
  - h5py
  - hdf5
  - numpy
  - pandas=0.25
  - pvapy=2.0.0
  - pyepics=3.4.0
  - python=3.7
  - scipy=1.3
# If the following variables are defined, they will be added
# to the /etc/profile.d/epics_env.sh file
# Note that the file is only created if epics_base_activate_profile
# is set to true
# epics_ca_addr_list: 192.168.1.255 192.168.2.255
# epics_ca_auto_addr_list: "NO"
# epics_ca_max_array_bytes: 10000000
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-epics-base
```

## License

BSD 2-clause
