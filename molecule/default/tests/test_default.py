import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_epics_base_env_created(host):
    assert host.file('/opt/conda/envs/epics/epics/lib/linux-x86_64/libca.so').exists
